const fs = require('fs')
const chalk = require('chalk')

const getNotes =  () => {'Your notes...' }

const addNote =  (title, body) => {
    const notes = loadNotes()
    // const duplicateNotes = notes.filter( (note) => note.title === title )
    const duplicateNote  = notes.find( (note) => note.title === title )

    if (!duplicateNote) {
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes)
        console.log( chalk.green.inverse('New note added!') )
    } else {
        console.log( chalk.red.inverse('Note title taken!') )
    }
}

const saveNotes =  (notes) => {
    const dataJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json', dataJSON)
}

const loadNotes =  () => {
    try {
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJSON = dataBuffer.toString()
        return JSON.parse(dataJSON)
    } catch (e) {
        return []
    }
}

const removeNote = (title) => {
    const notes = loadNotes()
    const notesToKeep = notes.filter( (note) =>  note.title !== title )
    if( notes.length === notesToKeep.length )
        console.log( chalk.bgRed("No note found!") )
    else{
        saveNotes(notesToKeep)
        console.log( chalk.bgGreen.black("Note removed!") )
    }
}

const listNotes = () => {
    const notes = loadNotes()
    console.log( chalk.inverse("Your notes") )
    // return notes.forEach( (note) => {
    //     console.table( "note: " + note.title + " " + note.body )
    // });
    console.table(notes)
}

const readNote = (title) => {
    const notes = loadNotes()
    const note = notes.find( (note) =>  note.title === title )

    if( note )
        console.log( chalk.green(note.title) + " " + note.body )
    else 
        console.log( chalk.bgRed("Error") )
}

module.exports = {
    getNotes: getNotes,
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote: readNote
}